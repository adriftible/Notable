# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Noteable has **3 release channels,**
- Release ✅ - Most stable, recomended for all.
- Canary ⛔ - Less stable, contains new *(somewhat stable)* features
- Staging 🚦 - Terrible performace and stability, contains new latest features

## 0.1.0 - ✅
Initial Release